﻿using Windows.Media.Streaming.Adaptive;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media;
using Youbora.adapter;
using Youbora.log;
using Youbora.util;

namespace Youbora.AdaptiveMediaAdapter
{
    public class AdaptiveMediaAdapter : Adapter
    {
        #region constructor
        private AdaptiveMediaSource mediaSource;
        private bool isErrored = false;
        private int acumErrorCount = 0;
        private string lastError = null;
        public void SetMediaSource(AdaptiveMediaSource ms)
        {
            this.mediaSource = ms;
        }

        public AdaptiveMediaAdapter(object playerref) : base(playerref) { }

        public override void RegisterListeners()
        {
            // Start Buffer Monitor
            Monitorplayhead(true, true);

            // Register events
            ((MediaElement)this.player).MediaEnded += Player_MediaEnded;
            ((MediaElement)this.player).MediaFailed += Player_MediaFailed;
            ((MediaElement)this.player).MediaOpened += Player_MediaOpened;
            ((MediaElement)this.player).CurrentStateChanged += Player_CurrentStateChanged;
        }

        public override void UnregisterListeners()
        {
            // UnRegister events
            if (player != null)
            {
                ((MediaElement)this.player).MediaEnded -= Player_MediaEnded;
                ((MediaElement)this.player).MediaFailed -= Player_MediaFailed;
                ((MediaElement)this.player).MediaOpened -= Player_MediaOpened;
                ((MediaElement)this.player).CurrentStateChanged -= Player_CurrentStateChanged;
            }

            // remove media source
            this.mediaSource = null;
        }
        #endregion
        #region eventHandlers
        private void Player_MediaOpened(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {
            Log.Debug("MediaOpened");
            FireJoin();
            acumErrorCount = 0;
            lastError = null;
        }

        private void Player_CurrentStateChanged(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {
            Log.Debug("State: " + ((MediaElement)this.player).CurrentState.ToString());
            if (((MediaElement)this.player).CurrentState == MediaElementState.Opening)
            {
                FireStart();
                isErrored = false;
            }
            else if (((MediaElement)this.player).CurrentState == MediaElementState.Paused)
            {
                FirePause();
            }
            else if (((MediaElement)this.player).CurrentState == MediaElementState.Playing)
            {
                if (!flags.isStarted)
                {
                    FireStart();
                    isErrored = false;
                    monitor.SkipNextTick();
                }
                FireResume();
                if (!flags.isJoined)
                {
                    FireJoin();
                    acumErrorCount = 0;
                    lastError = null;
                    monitor.SkipNextTick();
                }
            }
            else if (((MediaElement)this.player).CurrentState == MediaElementState.Stopped)
            {
                FireStop();
            }
        }

        private void Player_MediaFailed(object sender, Windows.UI.Xaml.ExceptionRoutedEventArgs e)
        {
            if (isErrored) return;
            if (e.ErrorMessage.Contains("6002")){
                acumErrorCount++;
                if (acumErrorCount != 5) return;
                acumErrorCount = 0;
            }
            if (lastError == e.ErrorMessage) return;
            lastError = e.ErrorMessage;
            FireError(e.ErrorMessage);
            FireStop();
            isErrored = true;
        }

        private void Player_MediaEnded(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {
            FireStop();
        }

        #endregion
        #region getters
        public override string GetVersion()
        {
            return "6.0.2-AdaptiveMedia-adapter-csharp";
        }

        public override double GetPlayhead()
        {
            return ((MediaElement)this.player).Position.TotalSeconds;
        }

        public override double GetPlayrate()
        {
            if (flags.isPaused) return 0;
            return (double) ((MediaElement)this.player).PlaybackRate;
        }

        public override double? GetDuration()
        {
            return ((MediaElement)this.player).NaturalDuration.TimeSpan.TotalSeconds;
        }

        public override double GetBitrate()
        {
            if (this.mediaSource != null)
            {
                return (double)this.mediaSource.CurrentPlaybackBitrate;
            }
            return -1;
        }

        public override double GetThroughput()
        {
            if (this.mediaSource != null)
            {
                return (double)this.mediaSource.CurrentDownloadBitrate;
            }
            return -1;
        }

        public override string GetRendition()
        {
            int height = ((MediaElement)this.player).NaturalVideoHeight;
            int width = ((MediaElement)this.player).NaturalVideoWidth;
            double bitrate = GetBitrate();
            return Util.BuildRenditionString(width, height, bitrate);
        }

        public override bool? GetIsLive()
        {
            if (this.mediaSource != null) {
                return this.mediaSource.IsLive;
            }
            return false;
        }

        public override string GetPlayerVersion()
        {
            var p = Windows.ApplicationModel.Package.Current.Id.Version;
            return p.Major + "." + p.Minor + "." + p.Build;
        }

        public override string GetPlayerName()
        {
            return "AdaptiveMedia";
        }
        #endregion
    }
}
